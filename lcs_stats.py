import sys
import asyncio
import websockets
import json
import csv
import time
from datetime import datetime

address = 'ws://livestats.proxy.lolesports.com/stats?jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2IjoiMS4wIiwiamlkIjoiODhjOGEwNDgtZmMzYS00YTI1LWJiNjItNDdiYWI5YjViMjdlIiwiaWF0IjoxNTE5ODM0MDM1MDIxLCJleHAiOjE1MjA0Mzg4MzUwMjEsIm5iZiI6MTUxOTgzNDAzNTAyMSwiY2lkIjoiYTkyNjQwZjI2ZGMzZTM1NGI0MDIwMjZhMjA3NWNiZjMiLCJzdWIiOnsiaXAiOiI4Mi42Ni4xMDEuMTAyIiwidWEiOiJNb3ppbGxhLzUuMCAoV2luZG93cyBOVCAxMC4wOyBXaW42NDsgeDY0OyBydjo1OS4wKSBHZWNrby8yMDEwMDEwMSBGaXJlZm94LzU5LjAifSwicmVmIjpbIndhdGNoLioubG9sZXNwb3J0cy5jb20iXSwic3J2IjpbImxpdmVzdGF0cy12MS4wIl19.gSB7og9mkU0jPPoVmPDp17erGEAjNs8yKXIhvOGSggY'

def get_ongoing_games(stats, game_times, new_game_times):
	ongoing_games = []
	print(game_times, new_game_times)
	if len(new_game_times) == len(game_times):
		for i in range (len(stats)):
			if game_times[i] != new_game_times[i]:
				ongoing_games.append(stats[i])
	return ongoing_games

def get_relevant_stats(ongoing_game):
	return [
			{
				'game id' : ongoing_game['generatedName'] if 'generatedName' in ongoing_game else ongoing_game['realm'],
				'game time': '{:.2f}'.format(ongoing_game['t'] // 1000 / 60),
				'player': player['summonerName'],
				'champion' : player['championName'],
				'level': player['level'],
				'kills': player['kills'],
				'deaths': player['deaths'],
				'assists': player['assists'],
				'wardsPlaced': player['wardsPlaced'],
				'wardsKilled': player['wardsKilled'],
				'creep score': player['mk'],
				'current gold': player['cg'],
				'total gold': player['tg'],
				'xp': player['xp'],
				'x': player['x'],
				'y': player['y'],
				'current health': player['h'],
				'current mana': player['p'],
				'total damage': player['td'],
				'physical damage': player['pd'],
				'magical damage': player['md'],
				'true damage': player['trd'],
				'total damage to champions': player['tdc'],
				'physical damage to champions': player['pdc'],
				'magical damage to champions': player['mdc'],
				'true damage to champions': player['trdc']
			}
			for player in list(ongoing_game['playerStats'].values())]

def write_to_csv_file(ongoing_game_stats, csv_file):
	fieldnames = ['game id', 'game time', 'player', 'champion', 'level', 'kills', 'deaths', 'assists', 'wardsPlaced', 'wardsKilled',
					'creep score', 'current gold', 'total gold', 'xp', 'x', 'y', 'current health', 'current mana',
					'total damage', 'physical damage', 'magical damage', 'true damage',
					'total damage to champions', 'physical damage to champions', 'magical damage to champions', 'true damage to champions']
	writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
	if csv_file.tell() == 0:
		writer.writeheader()
	writer.writerows(ongoing_game_stats)

def write_relevant_stats_to_csv_file(ongoing_games_stats, csv_file):
	for ongoing_game in ongoing_games_stats:
		ongoing_game_stats = get_relevant_stats(ongoing_game)
		write_to_csv_file(ongoing_game_stats, csv_file)
		# Console output below
		print( list(ongoing_game_stats[0].keys()) )
		for player_ongoing_stats in ongoing_game_stats:
			print( list(player_ongoing_stats.values()) )

async def write_ongoing_stats_to_csv_file(csv_file, game_times):
	async with websockets.connect(address) as websocket:
		json_stats = await websocket.recv()
		dict_stats = json.loads(json_stats)
		list_games_stats = [game for game in list(dict_stats.values()) if game != None]
		new_game_times = [game['t'] for game in list_games_stats]
		ongoing_games_stats = get_ongoing_games(list_games_stats, game_times, new_game_times)
		write_relevant_stats_to_csv_file(ongoing_games_stats, csv_file)
		# Console output below
		if len(ongoing_games_stats) > 0:
			print( list(list(ongoing_games_stats[0]['playerStats'].values())[0].keys()) )
		#print(ongoing_games)
	return new_game_times

def read_websocket_and_write_to_csv_infinite_loop(csv_file):
	game_times = []
	while True:
		try:
			game_times = asyncio.get_event_loop().run_until_complete(write_ongoing_stats_to_csv_file(csv_file, game_times))
		except Exception as e:
			with open('error_log.txt', 'a', newline='') as error_file:
				error_file.write('[{:%d/%m/%Y %H:%M:%S}] {:} | {:} {:}\n'.format(datetime.now(), e.__class__.__name__, e, game_times))
			print('Error: more details in error_log.txt.', file=sys.stderr)
		time.sleep(1)

def main():
	if len(sys.argv) == 2:
		with open(sys.argv[1], 'a', newline='') as csv_file:
			read_websocket_and_write_to_csv_infinite_loop(csv_file)
	else:
		print('Usage: python3 lcs_stats.py [filename]', file=sys.stderr)

if __name__ == '__main__':
	main()